﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using BackendlessAPI;
using BackendlessAPI.RT.Data;
using Xamarin.Forms;

namespace RTData
{
    public class MainVM : INotifyPropertyChanged
    {
        private ObservableCollection<Person> allItems = new ObservableCollection<Person>();
        public ObservableCollection<Person> AllItems { get => allItems; set => SetProperty(ref allItems, value, nameof(AllItems)); }

        //private IDataStore<Person> dataStore = Backendless.Data.Of<Person>();

        #region ENABLE RT DATA
        public void EnableRT()
        {
            IEventHandler<Person> eventHandler = Backendless.Data.Of<Person>().RT();
            Backendless.RT.AddConnectListener(async () =>
            {
                Debug.WriteLine("Start listening for RT Data");
            });

            Backendless.RT.AddConnectErrorListener((fault) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Connection cannot be established " + fault.Message, "Ok");
                });
            });

            //CREATE
            eventHandler.AddCreateListener(createdObject =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //await App.Current.MainPage.DisplayAlert("Event", $"Person {createdObject.Name} has been created. Object id: {createdObject.objectId}", "Ok");
                    AllItems.Add(createdObject);
                });
            });

            //UPDATE
            eventHandler.AddUpdateListener(receivedObject =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //await App.Current.MainPage.DisplayAlert("Event", $"Person {receivedObject.Name} has been created. Object id: {receivedObject.objectId}", "Ok");

                    var item = AllItems.FirstOrDefault(i => i.objectId.Equals(receivedObject.objectId));
                    if (item != null)
                    {
                        var index = AllItems.IndexOf(item);
                        if (index >= 0)
                            AllItems[index] = receivedObject;
                    }
                });
            });

        }
        #endregion

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    await RefreshItems();
                });
            }
        }
        async Task RefreshItems()
        {
            try
            {
                var list = await Backendless.Data.Of<Person>().FindAsync();
                AllItems = new ObservableCollection<Person>(list);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public ICommand CreateItemCommand
        {
            get
            {
                return new Command(async () => {
                    var userInput = await App.Current.MainPage.DisplayPromptAsync("Name", "enter a valid name");

                    await Backendless.Data.Of<Person>().SaveAsync(new Person(userInput));
                });
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
            {
                return false;
            }
            storage = value;
            OnPropertyChanged(propertyName);

            return true;
        }
        #endregion
    }
}
