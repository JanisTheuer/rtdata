﻿using System;
namespace RTData
{
    public class Person
    {
        public string objectId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Person()
        {
        }

        public Person(string name = null, int age = 0)
        {
            Name = name;
            Age = age;
        }
    }
}
