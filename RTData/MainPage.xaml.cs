﻿using Xamarin.Forms;

namespace RTData
{
    public partial class MainPage : ContentPage
    {
        MainVM viewModel = new MainVM();
        public MainPage()
        {
            InitializeComponent();
            BindingContext = viewModel;
        }

        void EnableRealTimeClicked(System.Object sender, System.EventArgs e)
        {
            RTButton.IsEnabled = false;

            viewModel.EnableRT();
        }
    }
}
