﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BackendlessAPI;

namespace RTData
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var appId = "************";
            var apiKey = "************";
            Backendless.InitApp(appId,apiKey);

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
